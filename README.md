# Commands

Make a new post `$ hugo new posts/title.md`

Start the hugo server (with drafts) `$ hugo serve -D`

Generate the public site `$ hugo`
