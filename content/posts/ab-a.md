---
title: "A"
projects: ["project-ab"]
date: 2019-02-11
tags: ["tag1", "tag2"]
categories: ["cat1", "tralala"]
---

## Content title

Lorem **ipsum** dolor sit *amet*, ***consectetur*** adipisicing elit. Quod a magnam voluptate officiis, doloribus. Repellat similique alias adipisci consequuntur, dolor necessitatibus voluptas ad harum nihil rem accusantium minima, odio asperiores.

{{< figure src="/images/image-001.jpg" title="Image caption" class="width-full" >}}

## Content subtitle

Lorem ipsum dolor sit amet, consectetur adipisicing elit. Illum quod consequuntur ad similique aut, esse doloremque expedita! Dolor, tempora totam natus nobis et possimus, architecto quasi praesentium molestias aut. Nostrum.

| Technique | Speed | Power |
------------|-------------| -------------
| Cut (red) | 25 | 80 |
| Engrave (black) | 500 | 25 |

### Content subsubtitle

- unordered list item
- unordered list item
- unordered list item
- unordered list item

{{< figure src="/images/image-001.jpg" title="Image caption" class="width-small" >}}
{{< figure src="/images/image-001.jpg" title="Image caption" class="width-small" >}}


### Content subsubtitle

1. ordered list item
2. ordered list item
3. ordered list item
4. ordered list item

> Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
